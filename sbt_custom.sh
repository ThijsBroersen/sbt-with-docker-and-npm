#!/bin/bash

docker run --rm \
  -v $PWD:/project -w /project \
  -e SBT_OPTS="-Dsbt.global.base=sbt-cache/.sbtboot -Dsbt.boot.directory=sbt-cache/boot -Dsbt.ivy.home=sbt-cache/.ivy -Dsbt.coursier.home=sbt-cache/.coursier" \
  -e SBT_CACHE_DIR="sbt-cache/.ivy/cache" \
  -e COURSIER_CACHE="sbt-cache/coursier" \
  -e DOCKER_HOST="tcp://docker:2375" \
  -e DOCKER_DRIVER=overlay2 \
  -v $PWD/sbt-cache:/project/sbt-cache \
  --user $(id -u):$(id -g) sbt-build-test:latest sbt $@
